# React
## A JavaScript library for building user interfaces

### A Simple Component

React компоненты реализуют метод `render()`, который принимает входные данные и возвращает то, что нужно отобразить. В этом примере используется XML-подобный синтаксис JSX. Входные данные, которые передаются в компонент, могут быть доступны в функции `render()` через `this.props`.

```
class HelloMessage extends React.Component {
  render() {
    return (
      <div>
        Hello {this.props.name}
      </div>
    );
  }
}

ReactDOM.render(
  <HelloMessage name="Taylor" />,
  mountNode
);
```

### A Stateful Component

В дополнение к входным данным (доступным через `this.props`), компонент может поддерживать внутренние данные состояния (доступ через `this.state`). Когда данные состояния компонента изменяются, разметка будет обновляться путем повторного вызова `render()`.

```
class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { seconds: 0 };
  }

  tick() {
    this.setState(prevState => ({
      seconds: prevState.seconds + 1
    }));
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div>
        Seconds: {this.state.seconds}
      </div>
    );
  }
}

ReactDOM.render(<Timer />, mountNode);
```

Используя `props` и `state`, мы можем создать небольшое приложение списка дел. В этом примере используется состояние для отслеживания текущего списка элементов, а также текста, введенного пользователем. Хотя обработчики событий оказываются встроенными, они собираются и реализуются с помощью делегирования событий.

```
class TodoApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = { items: [], text: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  render() {
    return (
      <div>
        <h3>TODO</h3>
        <TodoList items={this.state.items} />
        <form onSubmit={this.handleSubmit}>
          <input
            onChange={this.handleChange}
            value={this.state.text}
          />
          <button>
            Add #{this.state.items.length + 1}
          </button>
        </form>
      </div>
    );
  }

  handleChange(e) {
    this.setState({ text: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.text.length) {
      return;
    }
    const newItem = {
      text: this.state.text,
      id: Date.now()
    };
    this.setState(prevState => ({
      items: prevState.items.concat(newItem),
      text: ''
    }));
  }
}

class TodoList extends React.Component {
  render() {
    return (
      <ul>
        {this.props.items.map(item => (
          <li key={item.id}>{item.text}</li>
        ))}
      </ul>
    );
  }
}

ReactDOM.render(<TodoApp />, mountNode);
```

### The Component Lifecycle
Each component has several “lifecycle methods” that you can override to run code at particular times in the process. Methods prefixed with will are called right before something happens, and methods prefixed with did are called right after something happens.
#### Mounting
These methods are called when an instance of a component is being created and inserted into the DOM:
```
constructor()
componentWillMount()
render()
componentDidMount()
```
#### Updating
An update can be caused by changes to props or state. These methods are called when a component is being re-rendered:
```
componentWillReceiveProps()
shouldComponentUpdate()
componentWillUpdate()
render()
componentDidUpdate()
```
#### Unmounting
This method is called when a component is being removed from the DOM:
```componentWillUnmount()```
#### Error Handling
This method is called when there is an error during rendering, in a lifecycle method, or in the constructor of any child component.
```componentDidCatch()```
